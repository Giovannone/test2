CREATE TABLE MUSEI (
 NOMEM                                	    VARCHAR(30) NOT NULL,
 CITTA                                      VARCHAR(20));

 CREATE TABLE ARTISTI (
 NOMEA                                	    VARCHAR(30) NOT NULL,
 NAZIONALITA                                VARCHAR(20));

 CREATE TABLE OPERE (
 CODICE                                	    INT NOT NULL,
 TITOLO                                     VARCHAR(20),
 NOMEM                                      VARCHAR(30),
 NOMEA                                      VARCHAR(30));

 CREATE TABLE PERSONAGGI (
 PERSONAGGIO                                VARCHAR(30) NOT NULL,
 CODICE                                     INT);