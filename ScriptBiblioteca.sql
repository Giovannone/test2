USE [Biblioteca]
GO

/****** Object:  Table [dbo].[LIBRO]    Script Date: 17/11/2022 09:44:15 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[LIBRO](
	[Codice_Libro] [varchar](20) NOT NULL,
	[Titolo] [varchar](50) NOT NULL,
	[Genere] [varchar](20) NOT NULL,
	[Autore] [varchar](30) NOT NULL,
	[Scaffale] [varchar](20) NOT NULL,
	[Prestito] [bit] NOT NULL,
 CONSTRAINT [PK_Libro] PRIMARY KEY CLUSTERED 
(
	[Codice_Libro] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO

/****** Object:  Table [dbo].[PRESTITO]    Script Date: 17/11/2022 09:44:15 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[PRESTITO](
	[ID_Prestito] [varchar](10) NOT NULL,
	[Codice_Libro] [varchar](20) NOT NULL,
	[Codice_Tessera] [varchar](20) NOT NULL,
	[Data_inizio_prestito] [date] NOT NULL,
	[Data_fine_prestito] [date] NULL,
 CONSTRAINT [PK_Prestito] PRIMARY KEY CLUSTERED 
(
	[ID_Prestito] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO

/****** Object:  Table [dbo].[UTENTI]    Script Date: 17/11/2022 09:44:15 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[UTENTI](
	[Codice_Tessera] [varchar](20) NOT NULL,
	[Nome] [varchar](50) NOT NULL,
	[Cognome] [varchar](50) NOT NULL,
	[Data_di_nascita] [date] NULL,
 CONSTRAINT [PK_Utenti] PRIMARY KEY CLUSTERED 
(
	[Codice_Tessera] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO


